import { Router } from "express";
import users from "./secured/users";
import auth from "./auth";
import passport from 'passport'

const api = Router();

api.get("/", (req, res) => {
  res.json({
    name: "sanji.Api",
    meta: {
      version: "1.0.0",
      status: "running"
    }
  });
});

api.use("/auth", auth);
api.use("/users", passport.authenticate("jwt", {session: false }), users);

export default api;
