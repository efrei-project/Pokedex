import express from "express";
import bodyParser from "body-parser"
import api from "./routes";
import { db  as database} from "./models";
import passport from 'passport'
import './middleware/passport'

const start = async () => {
  
  try{
    
    const port = parseInt(process.argv[2] || 5000);
    const app = express();
    
    await database.authenticate()
    await database.sync({ force: false })
    
    app.use(passport.initialize())

    app.use(bodyParser.urlencoded({ extended: true }))
    app.use(bodyParser.json())

    app.get("/", (request, response) => {
      response.send("Please feel free to use our api with /api");
    });
    
    app.use("/api", api);
    
    app.listen(port, () => {
      console.log(`Server is running in localhost at ${port}`);
    })
  }catch (ex) {
    console.log("---", ex.message)
  }
};

start();
