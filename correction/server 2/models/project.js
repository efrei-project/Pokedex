import Sequelize, { Model } from 'sequelize'

export default class Project extends Model {
    static init(database) {
        return super.init({
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            name: {
                type: Sequelize.STRING,
                alloNul: false,
                validate: {
                    noEmpty: true
                }
            }
            },{
                tableName: "projects",
                sequelize: database,

                indexe: [
                    {
                        unique: true,
                        fields: ["id"]
                    }
                ]
            }
        )}
    }
