import Sequelize from "sequelize";
import Project from "./project"
import User from "./user";
// postgres://USER:PASS@HOST:PORT/DBNAME
// createdb sanji.dev

export const db = new Sequelize("postgres://adrien@localhost:5432/sanji.dev");

User.init(db)
Project.init(db)
