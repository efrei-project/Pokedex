import Sequelize, {Model} from 'sequelize';

export default class User extends Model{
  static init(database){
    return super.init(
      {
      uuid:{
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4
      },
      nickname:{
        type: Sequelize.STRING,
        allowNull: false,
        unique:{
          args: true,
          msg: "Nickname already in use"
        },
        validate:{
          isLongEnough(v){
            if(v.length < 5){
              throw new Error("Nickname must have at least 5 character")
            }
          }
        }
      },
      email:{
        type: Sequelize.STRING,
        allowNull: false,
        validate:{
          isEmail: true
        },
        unique:{
          args: true,
          msg: "Email already in use"
        }
      },
      password_digest:{
        type: Sequelize.STRING,
        allowNull: false,
        validate:{
          notEmpty: true
        }
      },
      password:{
        type: Sequelize.VIRTUAL,
        validate:{
          isLongEnough(v){
            if(v.length < 7){
              throw new Error("Password must have at least 7 character")
            }
          }
        }
      },
      password_confirmation:{
        type: Sequelize.VIRTUAL,
        validate:{
          isEqual(v){
            if(v != this.password){
              throw new Error("Password confirmation doesn't match")
            }
          }
        }
      }
    },
    {
      tableName: "users",
      sequelize: database,
      indexes: [{
        unique: true,
        fields: ["uuid", "nickname", "email"]
      }]
    })
  }
}
