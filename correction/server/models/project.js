import Sequelize, {Model} from 'sequelize';

export default class Project extends Model{
  static init(database){
    return super.init({
      id:{
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name:{
        type: Sequelize.STRING,
        alloNull: false
      }
    },
    {
      tableName: "projects",
      sequelize: database,
      indexes: [{
        unique: true,
        fields: ["id"]
      }]
    })
  }
}
