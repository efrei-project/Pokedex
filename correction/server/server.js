import express from "express";
import api from "./routes";
import {db as database} from "./models";

const start = () => {
  const port = parseInt(process.argv[2] || 5000);
  const app = express();

  database.authenticate();
  database.sync();
  app.get("/", (request, response) => {
    response.send("Please feel free to use our api with /api");
  });

  app.use("/api", api);

  app.listen(port, () => {
    console.log(`Server is running in localhost at ${port}`);
  });
};

// Let's Rock! :)
start();
