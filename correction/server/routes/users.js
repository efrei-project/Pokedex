import { Router } from "express";

const api = Router();

api.get("/", (req, res) => {
  res.json({
    users: []
  });
});

export default api;
