import { Router } from "express";
import users from "./users";

const api = Router();

api.get("/", (req, res) => {
  res.json({
    name: "sanji.Api",
    meta: {
      version: "1.0.0",
      status: "running"
    }
  });
});

api.use("/users", users);

export default api;
