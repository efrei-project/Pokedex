import React from 'react';
const Numeros = require('./Numeros.js')
const Names = require('./Names.js')

class NumNam extends React.Component{
  render(){
    return(
      <div className="NumNam">
        <Numeros pokemon={this.props.pokemon}/>
        <Names pokemon={this.props.pokemon}/>
      </div>
    )
  }
}
module.export = NumNam