import React from 'react';

class Carac extends React.Component{
  render(){
    const listCarac = [
      "espece",
      "taille",
      "poids",
      "fmratio",
      "oeufpas",
      "effortval",
      "type1",
      "type2",
      "expval",
      "expmax",
      "captureval",
      "groupoeuf1",
      "groupoeuf2",
      "capspe1",
      "capspe2",
      "capspe3",
      "couleur",
      "forme",
      "type",
      "sensib-combat",
      "sensib-feu",
      "sensib-électrique",
      "sensib-glace",
      "sensib-normal",
      "sensib-plante",
      "sensib-poison",
      "sensib-sol",
      "sensib-vol",
      "pokemon",
      "numero",
      "artwork_supp1-nom",
      "artwork_supp1-img",
      "artwork_supp2-nom",
      "artwork_supp2-img",
      "pdm",
      "diff_rs-rfvf",
      "diff_dp-pt",
      "diff_4G-fm"
    ]
    let stringToReturn = [];
    stringToReturn.push(Object.keys(this.props.pokemon).map((key, value) =>{
      if (listCarac.includes(key)){
        return key + " : " + value + " - ";
      } else {
        return ""
      }
    }))

    return(
      <div className="Carac">
      <br/>
      <br/>
      <br/>
        <p>
          {stringToReturn}
        </p>
      </div>
    )
  }
}
module.export = Carac