import React from 'react';
const Logo = require('./Logo.js')
const NumNam = require('./NumNam.js')

class ImgNumNam extends React.Component{
  render(){
    return(
      <div id="ImgNumNam" className="ImgNumNam">
        <Logo pokemon={this.props.pokemon}/>
        <NumNam pokemon={this.props.pokemon}/>
      </div>
    )
  }
}

module.export = ImgNumNam