import React from 'react';
const Ability = require('./Ability.js')

class Abilities extends React.Component{
  render(){
    const listAbility = this.props.pokemon.attaques.map((attaque) => {

      return <div key={attaque.nom}><Ability ability={attaque}/></div>
    })
    return(
      <div className="Abilities">
        {listAbility}
      </div>
    )
  }
}
module.export = Abilities