import React from 'react';
const ImgNumNam = require('./ImgNumNam.js')
const CarAbi = require('./CarAbi.js')

class Content extends React.Component{
  render(){
    if(this.props.pokemon < 0){
      return (
        <div>
          <img src="https://usercontent2.hubstatic.com/14144047.png" alt="mew"/>
          <p>Choisissez un pokémon ! </p>
        </div>
      )
    } else {
      return(

        <div className="Content">
          <ImgNumNam pokemon={this.props.pokemon[0]}/>
          <CarAbi pokemon={this.props.pokemon[0]}/>
        </div>
      )
    }
  }
}
module.export = Content