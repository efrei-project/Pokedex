import React from 'react';
import './App.css';
import axios from 'axios'

class Header extends React.Component{
  render(){
    const rout = 'header.jpg';
    return(
      <div id="header" className="container">
        <img src={rout} alt="nope"/>
      </div>
    )
  }
}

class Footer extends React.Component{
  render(){
    const rout = 'Footer.jpg';
    return(
      <div id="footer" className="container">
        <img src={rout} alt="nope"/>
      </div>
    )
  }
}
class Carac extends React.Component{
  render(){
    const listCarac = [
      "espece",
      "taille",
      "poids",
      "fmratio",
      "oeufpas",
      "effortval",
      "type1",
      "type2",
      "expval",
      "expmax",
      "captureval",
      "groupoeuf1",
      "groupoeuf2",
      "capspe1",
      "capspe2",
      "capspe3",
      "couleur",
      "forme",
      "type",
      "sensib-combat",
      "sensib-feu",
      "sensib-électrique",
      "sensib-glace",
      "sensib-normal",
      "sensib-plante",
      "sensib-poison",
      "sensib-sol",
      "sensib-vol",
      "pokemon",
      "numero",
      "artwork_supp1-nom",
      "artwork_supp1-img",
      "artwork_supp2-nom",
      "artwork_supp2-img",
      "pdm",
      "diff_rs-rfvf",
      "diff_dp-pt",
      "diff_4G-fm"
    ]
    let stringToReturn = [];
    stringToReturn.push(Object.keys(this.props.pokemon).map((key, value) =>{
      if (listCarac.includes(key)){
        return key + " : " + value + " - ";
      } else {
        return ""
      }
    }))

    return(
      <div className="Carac">
      <br/>
      <br/>
      <br/>
        <p>
          {stringToReturn}
        </p>
      </div>
    )
  }
}


class CarAbi extends React.Component{
  render(){
    return(
      <div className="CarAbi">
        <Carac pokemon={this.props.pokemon}/>
        <Abilities pokemon={this.props.pokemon}/>
      </div>
    )
  }
}
class Ability extends React.Component{
  render(){
    return(
      <div className="Ability">
        <p>
          Nom : {this.props.ability.nom} -
          Niveau : {this.props.ability.niveau} -
          Puissance : {this.props.ability.puissance} -
          Precision : {this.props.ability.precision} -
          PP : {this.props.ability.pp} -
        </p>
      </div>
    )
  }
}

class Abilities extends React.Component{
  render(){
    const listAbility = this.props.pokemon.attaques.map((attaque) => {

      return <div key={attaque.nom}><Ability ability={attaque}/></div>
    })
    return(
      <div className="Abilities">
        {listAbility}
      </div>
    )
  }
}
class Content extends React.Component{
  render(){
    if(this.props.pokemon < 0){
      return (
        <div>
          <img src="https://usercontent2.hubstatic.com/14144047.png" alt="mew"/>
          <p>Choisissez un pokémon ! </p>
        </div>
      )
    } else {
      return(

        <div className="Content">
          <ImgNumNam pokemon={this.props.pokemon[0]}/>
          <CarAbi pokemon={this.props.pokemon[0]}/>
        </div>
      )
    }
  }
}
class ImgNumNam extends React.Component{
  render(){
    return(
      <div id="ImgNumNam" className="ImgNumNam">
        <Logo pokemon={this.props.pokemon}/>
        <NumNam pokemon={this.props.pokemon}/>
      </div>
    )
  }
}
class Logo extends React.Component{
  render(){
    let number = this.props.pokemon.ndex;
    number = number[0] == 0 ? (number[1] == 0 ? number.substring(2) : number.substring(1)) : number;

    const url = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${number}.png`
    return(
      <div className="Logo">
        <img src={url} alt="there is a problem"/>
        <p id="legend">{this.props.pokemon.légende}</p>
      </div>
    )
  }
}

class Names extends React.Component{
  render(){
    return(
      <div className="Names">
        <p>
          Ndex : {this.props.pokemon.ndex} -
          Jdex : {this.props.pokemon.jdex} -
          Njdex : {this.props.pokemon.njdex} -
          Hdex : {this.props.pokemon.hdex} -
          Odex : {this.props.pokemon.odex} -
          Opdex : {this.props.pokemon.opdex}
        </p>
      </div>
    )
  }
}
class NumNam extends React.Component{
  render(){
    return(
      <div className="NumNam">
        <Numeros pokemon={this.props.pokemon}/>
        <Names pokemon={this.props.pokemon}/>
      </div>
    )
  }
}
class Numeros extends React.Component{
  render(){
    return(
      <div className="Numeros">
        <p>
          Nom : {this.props.pokemon.nom} -
          Nomen : {this.props.pokemon.nomen} -
          Nomja : {this.props.pokemon.nomja} -
          Nomtm : {this.props.pokemon.nomtm} -
          NomRomaji : {this.props.pokemon.nomromaji} -
          Nomde : {this.props.pokemon.nomde}
        </p>
      </div>
    )
  }
}

class SideMenu extends React.Component{
  constructor(props){
    super(props)
  }
  render(){
    const listPoke = this.props.pokemons.map((pokemon) => {
      return <button className="btn" onClick={this.props.handleClick} />
    })
    return (
      <div>
        {listPoke}
      </div>
    )
  }
}

class App extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      selectedPokemon: -1,
      pokemon: [],
      pokemons: []
    };
    this.handleClick = this.handleClick.bind(this);
  };

  compose(){
    axios.get(`http://localhost:3000/`, { crossdomain: true }).then(res =>{
      this.setState({pokemons: res.data})
    })
  }

  change(id){
    this.setSate({selectedPokemon: id})
  }

  getPokemon(id){
    let truc;
    axios.get("http://localhost:3000/pokemon?id=" + this.state.selectedPokemon).then(res =>{
    truc= res.data})
    return truc
  }


    handleClick(id) {
        this.setState({
            selectedPokemon: id,
            pokemon: this.getPokemon(id)
    });
    }

  render(){
    this.compose()
  if(this.state.selectedPokemon < 0){
    return (
        <div id="body">
          <div id="navmenu" className="container">
            <SideMenu pokemons={this.state.pokemons}/>
          </div>
          <div id="windows" className="container">
            <Header />
            <Content pokemon={this.state.selectedPokemon}/>
            <Footer />
          </div>
        </div>
      )
    } else {
    this.getPokemon()
      return (
        <div id="body">
          <div id="navmenu" className="container">
            <SideMenu pokemons={this.state.pokemons}/>
          </div>
          <div id="windows" className="container">
            <Header />
            <Content pokemon={this.state.pokemon}/>
            <Footer />
          </div>
        </div>
      )
    }
  }
}

export default App;
