import React from 'react';

class SideMenu extends React.Component{
  constructor(props){
    super(props)
  }
  render(){
    const listPoke = this.props.pokemons.map((pokemon) => {
      return <button className="btn" onClick={this.props.handleClick} />
    })
    return (
      <div>
        {listPoke}
      </div>
    )
  }
}

module.export = SideMenu