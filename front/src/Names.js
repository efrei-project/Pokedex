import React from 'react';

class Names extends React.Component{
  render(){
    return(
      <div className="Names">
        <p>
          Ndex : {this.props.pokemon.ndex} -
          Jdex : {this.props.pokemon.jdex} -
          Njdex : {this.props.pokemon.njdex} -
          Hdex : {this.props.pokemon.hdex} -
          Odex : {this.props.pokemon.odex} -
          Opdex : {this.props.pokemon.opdex}
        </p>
      </div>
    )
  }
}
module.export = Names