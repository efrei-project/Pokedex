import React from 'react';
const Carac = require('./Carac.js')
const Abilities = require('./Abilities.js')

class CarAbi extends React.Component{
  render(){
    return(
      <div className="CarAbi">
        <Carac pokemon={this.props.pokemon}/>
        <Abilities pokemon={this.props.pokemon}/>
      </div>
    )
  }
}

module.export = CarAbi