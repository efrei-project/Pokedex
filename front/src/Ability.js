import React from 'react';

class Ability extends React.Component{
  render(){
    return(
      <div className="Ability">
        <p>
          Nom : {this.props.ability.nom} -
          Niveau : {this.props.ability.niveau} -
          Puissance : {this.props.ability.puissance} -
          Precision : {this.props.ability.precision} -
          PP : {this.props.ability.pp} -
        </p>
      </div>
    )
  }
}
module.export = Ability