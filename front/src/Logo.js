import React from 'react';

class Logo extends React.Component{
  render(){
    let number = this.props.pokemon.ndex;
    number = number[0] == 0 ? (number[1] == 0 ? number.substring(2) : number.substring(1)) : number;

    const url = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${number}.png`
    return(
      <div className="Logo">
        <img src={url} alt="there is a problem"/>
        <p id="legend">{this.props.pokemon.légende}</p>
      </div>
    )
  }
}

module.export = Logo