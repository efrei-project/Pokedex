import React from 'react';

class Numeros extends React.Component{
  render(){
    return(
      <div className="Numeros">
        <p>
          Nom : {this.props.pokemon.nom} -
          Nomen : {this.props.pokemon.nomen} -
          Nomja : {this.props.pokemon.nomja} -
          Nomtm : {this.props.pokemon.nomtm} -
          NomRomaji : {this.props.pokemon.nomromaji} -
          Nomde : {this.props.pokemon.nomde}
        </p>
      </div>
    )
  }
}

module.export = Numeros