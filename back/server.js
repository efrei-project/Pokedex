const express = require("express");
const app = express();
const fs = require('fs');
const path = process.cwd();

const content = JSON.parse(fs.readFileSync(path + '/pokedex.json'));

app.use((req, res, next) =>{
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', (req, res) => {
  let arrayResult = [];

  content.forEach(pokemon => {
    arrayResult.push(
      {
        "id": pokemon.ndex,
        "name": pokemon.nom
      }
    )
  })
  arrayResult = arrayResult.sort((a,b) => {
    return (parseInt(a.id) - parseInt(b.id))
  })
  console.log(arrayResult);
// TODO Sort the array to get it sorted by id


  res.setHeader('Content-Type', 'application/json');
  res.json(arrayResult);
})

app.get('/pokemon', (req, res)=>{
  const id = req.query.id
  let arrFound = content.filter(pokemon => {
    return pokemon.ndex == id;
  });
  res.setHeader('Content-Type', 'application/json');
  res.json(arrFound);
})

const port = 80;
app.listen(port, ()=> console.log('Back-end listening on port ' + port))
